---
title: MY Fab Journey 2022
description: documentation for my fab journey
btn1:
    text: Link to Fab Academy
    url: https://fabacademy.org/

---

 I have been following **How to make (almost) everything** for quite some years and never had the time and money for it. In 2022 I decided to audit Fab Academy through their open repository. This way I can adjust the progress toward my schedule and needs. I also decided to setup a mini Fablab at home, which can take some extra time as well. The expected deadline I set for myself is Dec 31, quite generous also reasonable since I have to figure out a lot of things without local support.

